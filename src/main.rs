use std::{io};
use std::io::prelude::*;
use std::fmt::{Display, Formatter, Result};
use std::process::{exit};

#[derive(Copy, Clone)]
struct Point {
    x: f64,
    y: f64,
}


impl Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "Point ({},{})", self.x, self.y)
    }
}


fn f(x: &f64, y: &f64) -> f64 {
    f64::powf(*x - *y, 2_f64) + f64::powf(*x, 4_f64) + f64::powf(*y, 4_f64)
}

fn derivee_partielle_x(x: &f64, y: &f64) -> f64 {
    2_f64 * (*x) - 2_f64 * (*y) + 4_f64 * f64::powf(*x, 3_f64)
}

fn derivee_partielle_y(x: &f64, y: &f64) -> f64 {
    -2_f64 * (*x) + 2_f64 * (*y) + 4_f64 * f64::powf(*y, 3_f64)
}


fn phi_prime(p_point: &Point) -> f64 {
    let deriv_partielle_x = derivee_partielle_x(&p_point.x, &p_point.y);
    let deriv_partielle_y = derivee_partielle_y(&p_point.x, &p_point.y);
    let a = 4_f64 * deriv_partielle_x * deriv_partielle_x * deriv_partielle_x * deriv_partielle_x + 4_f64 * deriv_partielle_y * deriv_partielle_y * deriv_partielle_y * deriv_partielle_y;
    let b = 12_f64 * p_point.x * deriv_partielle_x * deriv_partielle_x * deriv_partielle_x + 12_f64 * p_point.y * deriv_partielle_y * deriv_partielle_y * deriv_partielle_y;
    let c = 2_f64 * deriv_partielle_x * deriv_partielle_x - 4_f64 * deriv_partielle_x * deriv_partielle_y + 2_f64 * deriv_partielle_y * deriv_partielle_y + 12_f64 * p_point.x * p_point.x * deriv_partielle_x * deriv_partielle_x + 12_f64 * p_point.y * p_point.y * deriv_partielle_y * deriv_partielle_y;
    let d = 2_f64 * p_point.x * deriv_partielle_x - 2_f64 * p_point.x * deriv_partielle_y - 2_f64 * p_point.y * deriv_partielle_x + 2_f64 * p_point.y * deriv_partielle_y + 4_f64 * p_point.x * p_point.x * p_point.x * deriv_partielle_x + 4_f64 * p_point.y * p_point.y * p_point.y * deriv_partielle_y;
    let p = (3_f64 * a * c - b * b) / (3_f64 * a * a);
    let q = (2_f64 * b * b * b - 9_f64 * a * b * c + 27_f64 * a * a * d) / (27_f64 * a * a * a);
    let racine_delta1 = (q * q + ((4_f64 * p * p * p) / 27_f64)).sqrt();
    (((-q - racine_delta1) / 2_f64).cbrt() + ((-q + racine_delta1) / 2_f64).cbrt() - (b / (3_f64 * a))).abs()
}


fn gradient_simple(p_point: Point, p_pas: f64) -> (Point, f32) {
    let mut point = p_point;
    let mut fonction_f = f(&point.x, &point.y);
    let d0_x = derivee_partielle_x(&point.x, &point.y);
    let d0_y = derivee_partielle_y(&point.x, &point.y);

    let mut ancienne_fonction_f: f64;
    let mut point_temp = Point {
        x: point.x,
        y: point.y,
    };
    let mut i = 0_f32;

    loop {
        i += 1_f32;
        ancienne_fonction_f = fonction_f;
        point_temp.x = point.x;
        point_temp.y = point.y;
        point.x = point.x + p_pas * (-d0_x);
        point.y = point.y + p_pas * (-d0_y);
        fonction_f = f(&point.x, &point.y);
        println!("Itération : {}", point_temp);
        if ancienne_fonction_f < fonction_f { break; }
    }
    (point_temp, i)
}

fn gradient_optimal(p_point: Point) -> (Point, f32) {
    let mut pas_optimal;
    let mut i = 0_f32;
    let mut ancienne_fonction_f;
    let mut point = p_point;
    let mut point_temp = Point {
        x: point.x,
        y: point.y,
    };
    let mut fonction_f = f(&point.x, &point.y);
    let mut deriv_x: f64;
    let mut deriv_y: f64;
    loop {
        i += 1_f32;
        deriv_x = -derivee_partielle_x(&point.x, &point.y);
        deriv_y = -derivee_partielle_y(&point.x, &point.y);
        pas_optimal = phi_prime(&point);
        ancienne_fonction_f = fonction_f;
        point_temp.x = point.x;
        point_temp.y = point.y;
        if pas_optimal.is_nan() {
            break;
        }
        point.x += (pas_optimal * deriv_x * 1000_f64).round() / 1000_f64;
        point.y += (pas_optimal * deriv_y * 1000_f64).round() / 1000_f64;
        fonction_f = f(&point.x, &point.y);
        println!("Itération : {}", point_temp);
        if ancienne_fonction_f <= fonction_f { break; }
    }
    (point_temp, i)
}

fn gradient_conjugue(p_point: Point) -> (Point, f32) {
    let mut point = p_point;
    let mut i = 0_f32;
    let mut pas_optimal;
    let mut fonction_f = f(&point.x, &point.y);
    let mut point_temp = Point {
        x: point.x,
        y: point.y,
    };

    let (mut beta_x, mut beta_y, mut norme_x, mut norme_y, mut ancienne_norme_x, mut ancienne_norme_y, mut ancienne_fonction_f): (f64, f64, f64, f64, f64, f64, f64);
    let (mut deriv_part_x, mut deriv_part_y): (f64, f64);

    norme_x = f64::powf(2_f64 * point.x, 2_f64) + f64::powf(2_f64 * point.y, 2_f64) + f64::powf(4_f64 * point.x, 3_f64);
    norme_y = f64::powf(2_f64 * point.x, 2_f64) + f64::powf(2_f64 * point.y, 2_f64) + f64::powf(4_f64 * point.y, 3_f64);

    deriv_part_x = derivee_partielle_x(&point.x, &point.y);
    deriv_part_y = derivee_partielle_y(&point.x, &point.y);

    loop {
        i += 1_f32;
        pas_optimal = phi_prime(&point);
        ancienne_fonction_f = fonction_f;
        point_temp.x = point.x;
        point_temp.y = point.y;
        if pas_optimal.is_nan() {
            break;
        }
        point.x += (pas_optimal * -deriv_part_x * 1000_f64).round() / 1000_f64;
        point.y += (pas_optimal * -deriv_part_y * 1000_f64).round() / 1000_f64;
        fonction_f = f(&point.x, &point.y);
        ancienne_norme_x = norme_x;
        ancienne_norme_y = norme_y;
        norme_x = f64::powf(2_f64 * point.x, 2_f64) + f64::powf(2_f64 * point.y, 2_f64) + f64::powf(4_f64 * point.x, 3_f64);
        norme_y = f64::powf(2_f64 * point.x, 2_f64) + f64::powf(2_f64 * point.y, 2_f64) + f64::powf(4_f64 * point.y, 3_f64);
        beta_x = norme_x / ancienne_norme_x;
        beta_y = norme_y / ancienne_norme_y;
        deriv_part_x = derivee_partielle_x(&point.x, &point.y) + beta_x * deriv_part_x;
        deriv_part_y = derivee_partielle_y(&point.x, &point.y) + beta_y * deriv_part_y;
        println!("Itération : {}", point_temp);
        if ancienne_fonction_f <= fonction_f { break; }
    }
    (point_temp, i)
}


fn polak_ribiere(p_point: Point) -> (Point, f32) {
    let mut point = p_point;
    let mut pas_optimal;
    let mut i = 0_f32;
    let mut fonction_f = f(&point.x, &point.y);
    let mut point_temp = Point {
        x: point.x,
        y: point.y,
    };

    let (mut beta_x, mut beta_y, mut norme_x, mut norme_y, mut ancienne_fonction_f): (f64, f64, f64, f64, f64);
    let (mut deriv_part_x, mut deriv_part_y): (f64, f64);

    deriv_part_x = derivee_partielle_x(&point.x, &point.y);
    deriv_part_y = derivee_partielle_y(&point.x, &point.y);

    loop {
        i += 1_f32;
        pas_optimal = phi_prime(&point);
        ancienne_fonction_f = fonction_f;
        point_temp.x = point.x;
        point_temp.y = point.y;
        if pas_optimal.is_nan() {
            break;
        }
        point.x += (pas_optimal * -deriv_part_x * 1000_f64).round() / 1000_f64;
        point.y += (pas_optimal * -deriv_part_y * 1000_f64).round() / 1000_f64;
        fonction_f = f(&point.x, &point.y);
        norme_x = f64::powf(2_f64 * point.x, 2_f64) + f64::powf(2_f64 * point.y, 2_f64) + f64::powf(4_f64 * point.x, 3_f64);
        norme_y = f64::powf(2_f64 * point.x, 2_f64) + f64::powf(2_f64 * point.y, 2_f64) + f64::powf(4_f64 * point.y, 3_f64);
        beta_x = (f64::powf(derivee_partielle_x(&point.x, &point.y), 2_f64) - (2_f64 * point_temp.x - 2_f64 * point_temp.y + 4_f64 * f64::powf(point_temp.x, 3_f64))) / norme_x;
        beta_y = (f64::powf(derivee_partielle_y(&point.x, &point.y), 2_f64) - (2_f64 * point_temp.x - 2_f64 * point_temp.y + 4_f64 * f64::powf(point_temp.y, 3_f64))) / norme_y;
        deriv_part_x = derivee_partielle_x(&point.x, &point.y) + beta_x * deriv_part_x;
        deriv_part_y = derivee_partielle_y(&point.x, &point.y) + beta_y * deriv_part_y;
        println!("Itération : {}", point_temp);
        if ancienne_fonction_f <= fonction_f { break; }
    }
    (point_temp, i)
}


fn main() {
    println!("----------------------- Programme d'optimisation -----------------------");
    println!("\t Par Pablo BONDIA-LUTTIAU");
    println!("\t La fonction étudiée est f(x,y) = (x-y)^4 + x^4 + y^4");

    let mut wants_continue = true;

    println!();
    println!("Saisie du point initial");
    let (mut coord_x, mut coord_y): (String, String);
    coord_x = String::new();
    coord_y = String::new();
    print!("Coordonnée x : ");
    io::stdout().flush().unwrap();
    let _ = std::io::stdin().read_line(&mut coord_x).unwrap();
    let x = match coord_x.trim().parse::<f64>() {
        Err(_) => {
            eprintln!("Mauvais format de nombre, veuillez réessayer");
            exit(-1);
        }
        Ok(num) => { num }
    };
    print!("Coordonnée y : ");
    io::stdout().flush().unwrap();
    let _ = std::io::stdin().read_line(&mut coord_y).unwrap();
    let y = match coord_y.trim().parse::<f64>() {
        Err(_) => {
            eprintln!("Mauvais format de nombre, veuillez réessayer");
            exit(-1);
        }
        Ok(num) => { num }
    };

    let p = Point {
        x,
        y,
    };


    loop {
        if !wants_continue { break; }
        let mut choice = String::new();
        println!();
        println!("1 - Méthode du gradient simple");
        println!("2 - Méthode du gradient optimal");
        println!("3 - Méthode du gradient conjugué");
        println!("4 - Méthode de Polak-Ribière");
        println!();
        print!("Quelle méthode voulez-vous appliquer (tout autre valeur fermera le programme) : ");
        io::stdout().flush().unwrap();
        let _ = std::io::stdin().read_line(&mut choice).unwrap();
        choice = choice.trim().parse().unwrap();
        match choice.as_str() {
            "1" => {
                println!("----- Méthode du gradient simple -----");
                print!("Choisir le pas (en format décimal) : ");
                io::stdout().flush().unwrap();
                let mut le_pas = String::new();
                let _ = std::io::stdin().read_line(&mut le_pas).unwrap();
                let pas = match le_pas.trim().parse::<f64>() {
                    Err(_) => {
                        eprintln!("Mauvais format de nombre, veuillez réessayer");
                        exit(-1);
                    }
                    Ok(num) => { num }
                };
                let (point_final, iterations) = gradient_simple(p, pas);
                println!("\t--> Minimum atteint au {} en {} itération(s)", point_final, iterations);
            }
            "2" => {
                let (point_final, iterations) = gradient_optimal(p);
                println!("\t--> Minimum atteint au {} en {} itération(s)", point_final, iterations);
            }
            "3" => {
                let (point_final, iterations) = gradient_conjugue(p);
                println!("\t--> Minimum atteint au {} en {} itération(s)", point_final, iterations);
            }
            "4" => {
                let (point_final, iterations) = polak_ribiere(p);
                println!("\t--> Minimum atteint au {} en {} itération(s)", point_final, iterations);
            }
            _ => {
                wants_continue = false;
            }
        }
    }
}
